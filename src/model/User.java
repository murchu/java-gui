package model;

// represents a tech support system user
public class User {

  public String name;
  public String password;
  public userRole role;

  public User(String name, String password, userRole role) {
    this.name = name;
    this.password = password;
    this.role = role;
  }

  enum userRole {
    SUPPORT, MANAGER, ADMIN
  }
}