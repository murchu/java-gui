package model;

import java.util.Date;

// represents a support ticket
public class Ticket {

  public int id;
  public Date dateOpened;
  public User assignee;
  public String detail;
  public issuePriority priority;

  public Ticket(String detail, issuePriority priority) {
    this.dateOpened = new Date();
    this.detail = detail;
    this.priority = priority;
  }

  public void setUser(User user) {
    this.assignee = user;
  }

  enum issuePriority {
    URGENT, NORMAL, LONGTERM;
  }
}