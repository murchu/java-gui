package codesnippets;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JTextField;

import java.sql.ResultSet;

public class ShowingData {
    public ShowingData(){
    
    	Connection conn = null;
    	Statement stmt = null;
    	ResultSet rs = null;
    	try {
    	    conn =
    	       DriverManager.getConnection("jdbc:mysql://localhost:3307/graham?" +
    	                                   "user=root&password=");

    	    // Do something with the Connection
    	    stmt = conn.createStatement();
    	    rs = stmt.executeQuery("select * from samplelogin;");

    	    // loop over results
    	    
    	    while(rs.next()){
    	    	String id = rs.getString("id");
    	        System.out.println(id);
    	    	
    	      
    	        String un = rs.getString("username");
    	        System.out.println(un);
    	    }
    	   
    	} catch (SQLException ex) {
    	    // handle any errors
    	    System.out.println("SQLException: " + ex.getMessage());
    	    System.out.println("SQLState: " + ex.getSQLState());
    	    System.out.println("VendorError: " + ex.getErrorCode());
    	}
    	
    	
    	
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
			new ShowingData();
	}

}
