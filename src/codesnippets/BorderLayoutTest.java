package codesnippets;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class BorderLayoutTest extends JFrame {

	public BorderLayoutTest() {
		
		this.setVisible(true);
		this.setSize(600,600);
		
		this.setLayout(new BorderLayout());
		
		JPanel top = new JPanel();
			JButton clickme = new JButton("Click me");
		    top.add(clickme);
		    
		    JButton anotherclick = new JButton("Other");
		    top.add(anotherclick);
		    this.add(top, BorderLayout.PAGE_START);
		
		JPanel bottom = new JPanel();
			JButton bottomButton = new JButton("Bottom Button");
		    bottom.add(bottomButton);
			this.add(bottom, BorderLayout.PAGE_END);
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
         new BorderLayoutTest();
	}

}
