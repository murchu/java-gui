package codesnippets;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class MainGUI2 extends JFrame implements ActionListener {

	JPanel buttonPanel = new JPanel();
	JButton showPanel = new JButton("Show Panel");
	
	public MainGUI2() {
		
		this.setTitle("Notepad");
		
		// Make the bar for the menu
		JMenuBar bar = new JMenuBar();
		this.setJMenuBar(bar);
		
			JMenu file = new JMenu("File");
			bar.add(file);
				JMenuItem open = new JMenuItem("Open");
				
				file.add(open);
				JMenuItem close = new JMenuItem("Close");
				file.add(close);
			
			JMenu about = new JMenu("About");
			bar.add(about);
			
				JMenuItem aboutus = new JMenuItem("About us");
				about.add(aboutus);
				
				JMenuItem moreinfo = new JMenuItem("More info");
				about.add(moreinfo);
		
		
		
		this.setSize(500,500);
		
		this.setVisible(true);
		
		// Making an instance of grid layout
		GridLayout gr = new GridLayout(2,1);
		// Setting a gap between the top and the bottom areas
		gr.setVgap(10);		
		
		this.setLayout(gr);
		
		// Add a top text area 
		JTextArea codeWindow = new JTextArea();
		this.add(codeWindow);
		
		// add a second text area
		JTextArea debugWindow = new JTextArea();
		this.add(debugWindow);
		
		
		
		
		repaint();
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
          new MainGUI();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getActionCommand().equals("test")) {
		    System.out.println("test button clicked!");
		}
		else if(e.getActionCommand().equals("test")) {
		    System.out.println("test button clicked!");
		}
		else if(e.getActionCommand().equals("show")) {
			
			showPanel.setVisible(false);
			this.remove(showPanel);
			
			this.add(buttonPanel);
			this.validate();
			this.repaint();
		}
		
		
		
		
		
	}

}
