package codesnippets;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

public class WindowListenerExample extends JFrame implements WindowListener {

	public WindowListenerExample() {
		
		setSize(300,300);
		setVisible(true);
		
		addWindowListener(this);
		
		repaint();
		validate();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
          new WindowListenerExample();
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
	
		System.out.println("window closed....");
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
	     System.out.println("Closing....");	
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println("Window Opened....");
	}

}
