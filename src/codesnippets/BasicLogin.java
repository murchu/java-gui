package codesnippets;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class BasicLogin extends JFrame implements ActionListener {
	JTextField username = null;
	JTextField password = null;
	
	public BasicLogin() {
		
		setSize(300,300);
		setVisible(true);
		
		this.setLayout(new GridLayout(3,2));
		
		JLabel un = new JLabel("Username");
		this.add(un);
		
		username = new JTextField(20);
		this.add(username);
		
		JLabel pw = new JLabel("Password");
		this.add(pw);
		password = new JTextField(20);
		this.add(password);
		
		JButton button = new JButton("Login!");
		button.addActionListener(this);
		this.add(button);
		
		validate();
		repaint();
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		BasicShowData d = new BasicShowData(this);
	}

public static void main(String[] args) {
    
    new BasicLogin();

  }
}
