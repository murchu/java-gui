package codesnippets;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class BasicShowData extends JFrame {

	public BasicShowData(BasicLogin ref) {
		
		String username = ref.username.getText();
		
		setSize(300,300);
		
		setVisible(true);
		
		JLabel tx = new JLabel("They logged in as: " + username);
		this.add(tx);
	}
}
