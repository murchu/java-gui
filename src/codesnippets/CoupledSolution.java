package codesnippets;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;


// Notice how in the coupled solution, the action listener needs
// to be implements for the ENTIRE JFrame
public class CoupledSolution extends JFrame implements ActionListener{

	
	public CoupledSolution(){
		
		setSize(500,500);
		setVisible(true);
		
		// Create the button as usual
		JButton btn2= new JButton("Click me!!");
		this.add(btn2);
		btn2.setActionCommand("okbutton");
		btn2.addActionListener(this);
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new CoupledSolution();
	}
	
	
	// Notice how in the COUPLED solution, we only have one 
	// action performed, but it has to have lots of IF statements to
	// find out exactly which butto has just been clicked!
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getActionCommand().equals("okbutton")){
			System.out.println("The button was clicked");
		}
		else if (e.getActionCommand().equals("cancel")){
			System.out.println("Cancel button clicked");
		}
	}

}
