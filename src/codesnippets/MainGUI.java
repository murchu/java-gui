package codesnippets;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class MainGUI extends JFrame implements ActionListener {

	public MainGUI() {
		
		this.setSize(500,500);
		
		this.setVisible(true);
		this.setLayout(new FlowLayout());
		
		// First button
		JButton test = new JButton("Sample Text"); // Making the button
		test.addActionListener(this); // turn the listening on
		test.setActionCommand("test"); // give it an ID
		this.add(test); // Adding the button to the JFrame
		
		
		// Second button
    JButton test2 = new JButton("More Sample Text"); // Making the button
    test2.addActionListener(this); // turn the listening on
    test2.setActionCommand("test2");
    this.add(test2); // Adding the button to the JFrame
		
    
		validate();
		repaint();
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getActionCommand().equals("test")) {
		    System.out.println("test button clicked!");
		}
		else if(e.getActionCommand().equals("test2")) {
		    System.out.println("test2 button clicked!");
		}
				
	}
	
	public static void main(String[] args) {
    // TODO Auto-generated method stub
          new MainGUI();
  }   

}
