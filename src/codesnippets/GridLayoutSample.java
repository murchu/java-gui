package codesnippets;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GridLayoutSample extends JFrame {

	public GridLayoutSample() {
		
		this.setSize(600,600);
		this.setVisible(true);
		
		// layout for the overall Jframe
		this.setLayout(new GridLayout(2,2));
		
		// panel with its own layout
		JPanel p1 = new JPanel();
			p1.setLayout(new FlowLayout());
			JButton button1 = new JButton("one");
			p1.add(button1);
			
			JButton secondButtonOnPanel = new JButton("second button");
			p1.add(secondButtonOnPanel);
			this.add(p1);
		
		JPanel p2 = new JPanel();
			JButton button2 = new JButton("two");
			p2.add(button2);
		this.add(p2);
		
		
		JButton button3 = new JButton("three");
		this.add(button3);
		
		JButton button4 = new JButton("four");
		this.add(button4);
		
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
         new GridLayoutSample();
	}

}
