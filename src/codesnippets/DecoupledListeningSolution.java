package codesnippets;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;


// Notice how this line does NOT implement the action listener
public class DecoupledListeningSolution extends JFrame {

	
	public DecoupledListeningSolution(){
		
		setSize(500,500);
		setVisible(true);
		
		// Create the button as usual
		JButton btn2= new JButton("Click me!!");
		this.add(btn2);
		// We are sticking an action listener and a public void actionPerformed
		// all directly onto the button.
		// The action performed only has one job, respond to just this button!
		btn2.addActionListener(new ActionListener() { 
  		@Override
  		public void actionPerformed(ActionEvent arg0) {
  			// TODO Auto-generated method stub
  			 System.out.println("The button has been clicked");
  		} 
		} );

		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
            new DecoupledListeningSolution();
	}

}
