package controller;

import model.Ticket;
import model.User;

// specifies the db actions that can be performed
public class DbController {


  // returns a user from the db
  User getUser(String username) {
    
  }

  // returns a ticket from the db
  Ticket getTicket(int ticketId) {

  }

  // returns all tickets from the db
  Ticket[] getAllTickets() {

  }

}