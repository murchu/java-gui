package view;

import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

// renders the support dashboard gui view
public class SupportDashboard extends JFrame implements ActionListener {

    ArrayList personsList;
    JFrame appFrame;
    JLabel jlbName, jlbAddress, jlbPhone, jlbEmail;
    JTextField jtfName, jtfAddress, jtfPhone, jtfEmail;
    JButton jbbSave, jbnDelete, jbnClear, jbnUpdate, jbnSearch,
            jbnForward, jbnBack, jbnExit;
    String name, address, email;
    int phone;
    int recordNumber; // used to naviagate using &gt;&gt; and  buttons
    Container cPane;
    public SupportDashboard() {

	  /*Create a frame, get its contentpane and set layout*/
        appFrame = new JFrame("Support Dashboard");
        cPane = appFrame.getContentPane();
        cPane.setLayout(new GridBagLayout());
        //Arrange components on contentPane and set Action Listeners to each JButton

        arrangeComponents();
        appFrame.setSize(500,300);
        appFrame.setResizable(false);
        appFrame.setVisible(true);

    }
    public void arrangeComponents() {
        jlbName = new JLabel("Name");
        jlbAddress = new JLabel("Address");
        jlbPhone = new JLabel("Phone");
        jlbEmail = new JLabel("Email");
        jtfName = new JTextField(20);
        jtfAddress = new JTextField(20);
        jtfPhone = new JTextField(20);
        jtfEmail = new JTextField(20);
        jbbSave = new JButton("Save");
        jbnDelete = new JButton("Delete");
        jbnClear = new JButton("Clear");
        jbnUpdate = new JButton("Update");
        jbnSearch = new JButton("Search");
        jbnForward = new JButton("&gt;&gt;");
        jbnBack = new JButton("");
        jbnExit = new JButton("Exit");
			/*add all initialized components to the container*/
        GridBagConstraints gridBagConstraintsx01 = new GridBagConstraints();
        gridBagConstraintsx01.gridx = 0;
        gridBagConstraintsx01.gridy = 0;
        gridBagConstraintsx01.insets = new Insets(5, 5, 5, 5);
        cPane.add(jlbName, gridBagConstraintsx01);
        GridBagConstraints gridBagConstraintsx02 = new GridBagConstraints();
        gridBagConstraintsx02.gridx = 1;
        gridBagConstraintsx02.insets = new Insets(5, 5, 5, 5);
        gridBagConstraintsx02.gridy = 0;
        gridBagConstraintsx02.gridwidth = 2;
        gridBagConstraintsx02.fill = GridBagConstraints.BOTH;
        cPane.add(jtfName, gridBagConstraintsx02);
        GridBagConstraints gridBagConstraintsx03 = new GridBagConstraints();
        gridBagConstraintsx03.gridx = 0;
        gridBagConstraintsx03.insets = new Insets(5, 5, 5, 5);
        gridBagConstraintsx03.gridy = 1;
        cPane.add(jlbAddress, gridBagConstraintsx03);
        GridBagConstraints gridBagConstraintsx04 = new GridBagConstraints();
        gridBagConstraintsx04.gridx = 1;
        gridBagConstraintsx04.insets = new Insets(5, 5, 5, 5);
        gridBagConstraintsx04.gridy = 1;
        gridBagConstraintsx04.gridwidth = 2;
        gridBagConstraintsx04.fill = GridBagConstraints.BOTH;
        cPane.add(jtfAddress, gridBagConstraintsx04);
        GridBagConstraints gridBagConstraintsx05 = new GridBagConstraints();
        gridBagConstraintsx05.gridx = 0;
        gridBagConstraintsx05.insets = new Insets(5, 5, 5, 5);
        gridBagConstraintsx05.gridy = 2;
        cPane.add(jlbPhone, gridBagConstraintsx05);
        GridBagConstraints gridBagConstraintsx06 = new GridBagConstraints();
        gridBagConstraintsx06.gridx = 1;
        gridBagConstraintsx06.gridy = 2;
        gridBagConstraintsx06.insets = new Insets(5, 5, 5, 5);
        gridBagConstraintsx06.gridwidth = 2;
        gridBagConstraintsx06.fill = GridBagConstraints.BOTH;
        cPane.add(jtfPhone, gridBagConstraintsx06);
        GridBagConstraints gridBagConstraintsx07 = new GridBagConstraints();
        gridBagConstraintsx07.gridx = 0;
        gridBagConstraintsx07.insets = new Insets(5, 5, 5, 5);
        gridBagConstraintsx07.gridy = 3;
        cPane.add(jlbEmail, gridBagConstraintsx07);
        GridBagConstraints gridBagConstraintsx08 = new GridBagConstraints();
        gridBagConstraintsx08.gridx = 1;
        gridBagConstraintsx08.gridy = 3;
        gridBagConstraintsx08.gridwidth = 2;
        gridBagConstraintsx08.insets = new Insets(5, 5, 5, 5);
        gridBagConstraintsx08.fill = GridBagConstraints.BOTH;
        cPane.add(jtfEmail, gridBagConstraintsx08);
        GridBagConstraints gridBagConstraintsx09 = new GridBagConstraints();
        gridBagConstraintsx09.gridx = 0;
        gridBagConstraintsx09.gridy = 4;
        gridBagConstraintsx09.insets = new Insets(5, 5, 5, 5);
        cPane.add(jbbSave, gridBagConstraintsx09);
        GridBagConstraints gridBagConstraintsx10 = new GridBagConstraints();
        gridBagConstraintsx10.gridx = 1;
        gridBagConstraintsx10.gridy = 4;
        gridBagConstraintsx10.insets = new Insets(5, 5, 5, 5);
        cPane.add(jbnDelete, gridBagConstraintsx10);
        GridBagConstraints gridBagConstraintsx11 = new GridBagConstraints();
        gridBagConstraintsx11.gridx = 2;
        gridBagConstraintsx11.gridy = 4;
        gridBagConstraintsx11.insets = new Insets(5, 5, 5, 5);
        cPane.add(jbnUpdate, gridBagConstraintsx11);
        GridBagConstraints gridBagConstraintsx12 = new GridBagConstraints();
        gridBagConstraintsx12.gridx = 0;
        gridBagConstraintsx12.gridy = 5;
        gridBagConstraintsx12.insets = new Insets(5, 5, 5, 5);
        cPane.add(jbnBack, gridBagConstraintsx12);
        GridBagConstraints gridBagConstraintsx13 = new GridBagConstraints();
        gridBagConstraintsx13.gridx = 1;
        gridBagConstraintsx13.gridy = 5;
        gridBagConstraintsx13.insets = new Insets(5, 5, 5, 5);
        cPane.add(jbnSearch, gridBagConstraintsx13);
        GridBagConstraints gridBagConstraintsx14 = new GridBagConstraints();
        gridBagConstraintsx14.gridx = 2;
        gridBagConstraintsx14.gridy = 5;
        gridBagConstraintsx14.insets = new Insets(5, 5, 5, 5);
        cPane.add(jbnForward, gridBagConstraintsx14);
        GridBagConstraints gridBagConstraintsx15 = new GridBagConstraints();
        gridBagConstraintsx15.gridx = 1;
        gridBagConstraintsx15.insets = new Insets(5, 5, 5, 5);
        gridBagConstraintsx15.gridy = 6;
        cPane.add(jbnClear, gridBagConstraintsx15);
        GridBagConstraints gridBagConstraintsx16 = new GridBagConstraints();
        gridBagConstraintsx16.gridx = 2;
        gridBagConstraintsx16.gridy = 6;
        gridBagConstraintsx16.insets = new Insets(5, 5, 5, 5);
        cPane.add(jbnExit, gridBagConstraintsx16);
        jbbSave.addActionListener(this);
        jbnDelete.addActionListener(this);
        jbnClear.addActionListener(this);
        jbnUpdate.addActionListener(this);
        jbnSearch.addActionListener(this);
        jbnForward.addActionListener(this);
        jbnBack.addActionListener(this);
        jbnExit.addActionListener(this);

    }

    public void actionPerformed(ActionEvent e) {

        // if you want to set a label for each of the buttons
        // and then redirect the user to a different part of the program
        // you can use the getActionCommand to check which button
        // has sent the request
        if (e.getSource() == jbnDelete) {
            // add action
        }

    }

    public static void main(String[] args) {
        // Creating new instance of this class
        new SupportDashboard();

    }

}
