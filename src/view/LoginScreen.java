package view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.sql.ResultSet;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;


// renders the login gui view
public class LoginScreen extends JFrame implements ActionListener {
	
	JTextField id = null;
	JTextField password = null;

	ArrayList personsList;
	JFrame appFrame;
	JLabel jlbName;
	JLabel jlbPassword;

	JTextField jtfName;
	JTextField jtfPassword;

	JButton jbnLogin;
	String name, address, email;
	int phone;
	int recordNumber; // used to naviagate using &gt;&gt; and  buttons
	Container cPane;

	//Default constructor for this class
	public LoginScreen(){
		/*Create a frame, get its contentpane and set layout*/
    appFrame = new JFrame("Tech Support System");
    cPane = appFrame.getContentPane();
    cPane.setLayout(new GridBagLayout());

    //Arrange components on contentPane and set Action Listeners to each JButton
    arrangeComponents();
    appFrame.setSize(400,300);
    appFrame.setResizable(false);
    appFrame.setVisible(true);
		
	}
	
	public void arrangeComponents() {
    jlbName = new JLabel("Username");
    jlbPassword = new JLabel("Password");

    jtfName = new JTextField(20);
    jtfPassword = new JTextField(20);

    jbnLogin = new JButton("Login");

    
    /*add all initialized components to the container*/
    GridBagConstraints gridBagConstraintsx01 = new GridBagConstraints();
    gridBagConstraintsx01.gridx = 0;
    gridBagConstraintsx01.gridy = 0;
    gridBagConstraintsx01.insets = new Insets(5, 5, 5, 5);
    cPane.add(jlbName, gridBagConstraintsx01);

    GridBagConstraints gridBagConstraintsx02 = new GridBagConstraints();
    gridBagConstraintsx02.gridx = 1;
    gridBagConstraintsx02.insets = new Insets(5, 5, 5, 5);
    gridBagConstraintsx02.gridy = 0;
    gridBagConstraintsx02.gridwidth = 2;
    gridBagConstraintsx02.fill = GridBagConstraints.BOTH;
    cPane.add(jtfName, gridBagConstraintsx02);

    GridBagConstraints gridBagConstraintsx03 = new GridBagConstraints();
    gridBagConstraintsx03.gridx = 0;
    gridBagConstraintsx03.insets = new Insets(5, 5, 5, 5);
    gridBagConstraintsx03.gridy = 1;
    cPane.add(jlbPassword, gridBagConstraintsx03);

    GridBagConstraints gridBagConstraintsx04 = new GridBagConstraints();
    gridBagConstraintsx04.gridx = 1;
    gridBagConstraintsx04.insets = new Insets(5, 5, 5, 5);
    gridBagConstraintsx04.gridy = 1;
    gridBagConstraintsx04.gridwidth = 2;
    gridBagConstraintsx04.fill = GridBagConstraints.BOTH;
    cPane.add(jtfPassword, gridBagConstraintsx04);
    
    GridBagConstraints gridBagConstraintsx11 = new GridBagConstraints();
    gridBagConstraintsx11.gridx = 2;
    gridBagConstraintsx11.gridy = 4;
    gridBagConstraintsx11.insets = new Insets(5, 5, 5, 5);
    cPane.add(jbnLogin, gridBagConstraintsx11);
    
    jbnLogin.addActionListener(this);
 
	}
	
	//This is the action listener. In here we indicate what to do when different buttons are pressed.
	@Override
	public void actionPerformed(ActionEvent e) {
		
		// if you want to set a label for each of the buttons
		// and then redirect the user to a different part of the program
		// you can use the getActionCommand to check which button
		// has sent the request
		if (e.getSource() == jbnLogin) {
			loginWithDatabase();
		}
			
	}

	//This is the method that we call when the login button is pressed
	public void loginWithDatabase(){
		
		//Creating an instance of the database connector controller.
		try {
			
		  Class.forName("com.mysql.jdbc.Driver").newInstance();
		
		}catch(Exception e ){}
		
		//Declaring the variables that we'll need.
		Connection conn = null;
    	Statement stmt = null;
    	ResultSet rs = null;
    	
    	//Performing the actual connection
    	try {
    	    conn =
    	       DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/gui?user=root&password=");

    	    // Do something with the Connection
    	    stmt = conn.createStatement();

    	    // or alternatively, if you don't know ahead of time that
    	    // the query will be a SELECT...
    	    String idd = id.getText();
    	    String pw = password.getText();
    	    
    	    //Executing the query
    	    stmt.execute("select * from user where id = '"+idd+"' and password = '"+pw+"'");
    	    
    	    //Placing the results in a variable
    	    rs = stmt.getResultSet();
    	    
    	    //Now we evaluate the results
    	    if(!rs.next()) { //if it is empty, show a dialog saying the user doen't exists.
    	    	JOptionPane.showMessageDialog(this, "That user doen't exist");
    	    	
    	    }
    	    else { //If we have in did one result
    	    	//We can evaluate the type of the user
    	    	if (rs.getString("type").equals("admin")) { // If it is admin, create a new instance of the class admin
    	    		new Admin(rs.getString("name"));
    	    		
    	    	}
    	    	else if (rs.getString("type").equals("manager")) { // if it is manager, create a new instance of the class Manager
    	    		new Manager(rs.getString("name"));
    	    		
    	    	}
    	    	
    	    }

    	} catch (SQLException ex) {
    	    // handle any errors
    	    System.out.println("SQLException: " + ex.getMessage());
    	    System.out.println("SQLState: " + ex.getSQLState());
    	    System.out.println("VendorError: " + ex.getErrorCode());
    	}
    	
	}

	public static void main(String[] args) {
		// Creating new instance of this class
		new LoginScreen();
		
	}
}
